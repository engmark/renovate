let
  pkgs = import (builtins.fetchTarball {
    name = "nixos-unstable-2024-11-09";
    url = "https://github.com/nixos/nixpkgs/archive/85f7e662eda4fa3a995556527c87b2524b691933.tar.gz";
    sha256 = "1p8qam6pixcin63wai3y55bcyfi1i8525s1hh17177cqchh1j117";
  }) { };
in
pkgs.mkShell {
  packages = [
    pkgs.nixfmt-rfc-style
    pkgs.cacert
    pkgs.gitFull
    pkgs.gitlint
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.python3Packages.jsonschema
    pkgs.statix
  ];
}
